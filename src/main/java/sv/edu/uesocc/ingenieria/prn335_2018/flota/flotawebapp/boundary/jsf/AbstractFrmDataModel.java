package sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.boundary.jsf;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.control.AbstractFacade;

/**
 *
 * @author cristian
 * @param <T>
 */
public abstract class AbstractFrmDataModel<T> {

    protected EstadoCRUD estado;
    private LazyDataModel<T> modelo;
    protected T registro;

    @PostConstruct
    public void inicializar() {
        crearNuevo();
        this.estado = EstadoCRUD.NUEVO;
        this.inicializarModelo();
        this.modelo.setRowIndex(-1);

    }

    public abstract Object clavePorDatos(T object);

    public abstract T datosPorClave(String rowkey);

    protected int pageSize = 5;

    public abstract AbstractFacade getFacade();

    public abstract void crearNuevo();
    
    
    public List<T> cargarDatos(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        //le agregue el <T>
        List<T> salida = null;
        try {
            if (getFacade() != null) {
                salida = getFacade().findRange(first, pageSize);
                if (this.modelo != null) {
                    this.modelo.setRowCount(getFacade().count());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            if (salida == null) {
                salida = new ArrayList();
            }
        }
        return salida;
    }

    protected void inicializarModelo() {
        try {
            this.modelo = new LazyDataModel<T>() {
                @Override
                public Object getRowKey(T object) {
                    return clavePorDatos(object);
                }

                @Override
                public T getRowData(String rowKey) {
                    return datosPorClave(rowKey);
                }

                @Override
                public List<T> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    return cargarDatos(first, pageSize, sortField, sortOrder, filters);
                }
            };

        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

    }

    public void btnNuevo(ActionEvent ae) {
        this.estado = EstadoCRUD.CREAR;
    }

    public void btnAgregar(ActionEvent ae) {
        this.estado = EstadoCRUD.NUEVO;
    }

    public void btnModificar(ActionEvent ae) {
        this.estado = EstadoCRUD.NUEVO;
    }

    public void btnEliminarHandler(ActionEvent ae) {
        this.estado = EstadoCRUD.NUEVO;
    }

    public void btnCancelarHandler(ActionEvent ae) {
        this.estado = EstadoCRUD.NUEVO;
    }

    public void onRowSelected(SelectEvent se) {

        if (se.getObject() != null) {
            try {
                this.registro = (T) se.getObject();
                this.estado = EstadoCRUD.EDITAR;
            } catch (Exception ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
    }

    public void onRowDeselect(UnselectEvent event) {
        this.estado = EstadoCRUD.NUEVO;
        crearNuevo();
        this.modelo.setRowIndex(-1);

    }

    public LazyDataModel<T> getModelo() {
        return modelo;
    }

    public void setModelo(LazyDataModel<T> model) {
        this.modelo = model;
    }

    public enum EstadoCRUD {
        NUEVO, CREAR, EDITAR;
    }

    public EstadoCRUD getEstado() {
        return estado;
    }

    public void setEstado(EstadoCRUD estado) {
        this.estado = estado;
    }

    public T getRegistro() {
        return registro;
    }

    public void setRegistro(T registro) {
        this.registro = registro;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
