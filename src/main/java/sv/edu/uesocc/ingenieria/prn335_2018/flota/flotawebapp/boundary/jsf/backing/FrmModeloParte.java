package sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.boundary.jsf.backing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Modelo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.ModeloParte;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Parte;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.boundary.jsf.AbstractFrmDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.control.AbstractFacade;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.control.ModeloFacade;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.control.ModeloParteFacade;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.flotawebapp.control.ParteFacade;

/**
 *
 * @author cristian
 */
@Named
@ViewScoped
class FrmModeloParte extends AbstractFrmDataModel<ModeloParte> implements Serializable{

    @Inject
    ModeloParteFacade modeloFacade;
    @Inject
    ParteFacade parteFacade;

    private List<ModeloParte> modeloPartList;

    public FrmModeloParte() {
    }
    @PersistenceContext(unitName = "flota")
    EntityManager em;
    
//Consulta: SELECT m FROM ModeloParte m WHERE m.idModelo.idModelo ="el idmodelo del registro que nos envian"

    public FrmModeloParte(ModeloParteFacade modeloParteFacade, ModeloFacade modeloFacade, ParteFacade parteFacade, Modelo registro) {
        modeloPartList = new ArrayList<>();
        String jpql = "SELECT m FROM ModeloParte m WHERE m.idModelo.idModelo =" + registro.getIdModelo();
        Query query = em.createQuery(jpql);
        modeloPartList = query.getResultList();
    }
    
    private String inputString;
    
//     public List<String> nameSuggestions(String enteredValue) {
//        List<String> matches = new ArrayList<>();
//        List<Parte> parteList = parteFacade.findAll();
//        //using data factory for getting suggestions
//        for (String s : new Parte().getNombre()) {
//            if (s.toLowerCase().startsWith(enteredValue.toLowerCase())) {
//                matches.add(s);
//            }
//        }
//        return matches;
//    }

    public String getInputString() {
        return inputString;
    }

    public void setInputString(String inputString) {
        this.inputString = inputString;
    }
    
    

    @PostConstruct
    @Override
    public void inicializar() {
        super.inicializar();
        try {
            this.modeloPartList = modeloFacade.findAll();
        } catch (Exception ex) {
            this.modeloPartList = Collections.EMPTY_LIST;
        }

    }

    @Override
    public Object clavePorDatos(ModeloParte object) {
        if (object != null) {
            return object.getIdModeloParte();
        }
        return null;
    }

    @Override
    public ModeloParte datosPorClave(String rowkey) {
        if (rowkey != null && !rowkey.trim().isEmpty()) {
            try {
                return this.getModelo().getWrappedData().stream().filter(r -> r.getIdModeloParte().toString().compareTo(rowkey) == 0).collect(Collectors.toList()).get(0);
            } catch (Exception ex) {

            }
        }
        return null;
    }

    @Override
    public AbstractFacade getFacade() {
        return modeloFacade;
    }

    @Override
    public void crearNuevo() {
        this.registro = new ModeloParte();
    }

}
